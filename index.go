package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"math/big"
	"os"
	"path/filepath"
	"time"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage:", os.Args[0], "DIR")
		return
	}
	dir := os.Args[1]
	err := filepath.Walk(dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() {
				f, err := os.Open(path)
				if err != nil {
					log.Fatal(err)
					return err
				}
				defer f.Close()

				h := sha256.New()
				if _, err := io.Copy(h, f); err != nil {
					log.Fatal(err)
					return err
				}

				hashInt := new(big.Int).SetBytes(h.Sum(nil))
				hashIntSmall := new(big.Int)
				mod := new(big.Int).SetInt64(1000 * 60 * 60 * 24 * 365 * 30)
				hashIntSmall = hashIntSmall.Mod(hashInt, mod)
				//hashIntSmall = hashIntSmall.SetInt64(1)
				//dur := new(big.Int).SetInt64(1000 * 60 * 60 * 24 * 365 * 30)
				hashTime := time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC).Add(time.Duration(
					hashIntSmall.Int64() * 1000000,
				))
				//fmt.Println(path, info.Size(), hashTime)
				fmt.Println(path, hashTime)
				//fmt.Printf("%x\r\n", h.Sum(nil))

				err = os.Chtimes(path, hashTime, hashTime)
				if err != nil {
					fmt.Println(err)
					return err
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}
